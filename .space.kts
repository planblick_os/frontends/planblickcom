/**
* JetBrains Space Automation
* This Kotlin-script file lets you automate build activities
* For more info, see https://www.jetbrains.com/help/space/automation.html
*/

// STAGING
job("Build, push and deploy staging") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("staging")
            }
        }
    }

    container(displayName = "Set deployment", image = "openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                project = api.projectIdentifier(),
                targetIdentifier = TargetIdentifier.Key("planblickcom-staging"),
                version = System.getenv("JB_SPACE_GIT_REVISION"),
                // automatically update deployment status based on a status of a job
                syncWithAutomationJob = true
            )
        }
    }

    docker {
        build {
            file = "./Dockerfile"
            labels["vendor"] = "planBLICK GmbH"
        }

        push("planblick.registry.jetbrains.space/p/planblick/images/planblickcom") {
            // use current job run number as a tag - '0.0.run_number'
            tags("\$JB_SPACE_GIT_REVISION")
            tags("staging")
            // see example on how to use branch name in a tag
        }
    }

    container(displayName = "Trigger deployment on staging kubernetes cluster", image = "registry.gitlab.com/crowdsoft-foundation/various/pytestkube:latest") {
        env["KUBECONFIG_STAGING"] = Secrets("kubeconfig_staging")
        shellScript {
            content = """
                find ./. -type f \( -name '*.yaml' \) -print0 | xargs -0 sed -i "s/\[version\]/${'$'}JB_SPACE_GIT_REVISION/g"
                    echo "${'$'}KUBECONFIG_STAGING" > ./kube_config.yaml
                    cat ./planblick_staging.yaml
                    kubectl apply -f ./planblick_staging.yaml --kubeconfig="./kube_config.yaml"
                    rm -f ./kube_config.yaml
            """
        }
    }
}

// PRODUCTION

job("Build, push and deploy production") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("production")
            }
        }
    }
    container(displayName = "Set deployment", image = "openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                project = api.projectIdentifier(),
                targetIdentifier = TargetIdentifier.Key("planblickcom-production"),
                version = System.getenv("JB_SPACE_GIT_REVISION"),
                // automatically update deployment status based on a status of a job
                syncWithAutomationJob = true
            )
        }
    }

    docker() {
        build {
            file = "./Dockerfile"
            labels["vendor"] = "planBLICK GmbH"
        }

        push("planblick.registry.jetbrains.space/p/planblick/images/planblickcom") {
            // use current job run number as a tag - '0.0.run_number'
            tags("\$JB_SPACE_GIT_REVISION")
            tags("production")
            // see example on how to use branch name in a tag
        }
    }

    container(displayName = "Trigger deployment on production kubernetes cluster", image = "registry.gitlab.com/crowdsoft-foundation/various/pytestkube:latest") {
        env["KUBECONFIG_PRODUCTION"] = Secrets("kubeconfig_production")
        shellScript {
            content = """
                find ./. -type f \( -name '*.yaml' \) -print0 | xargs -0 sed -i "s/\[version\]/${'$'}JB_SPACE_GIT_REVISION/g"
                echo "${'$'}KUBECONFIG_PRODUCTION" > ./kube_config.yaml
                cat ./planblick.yaml
                kubectl apply -f ./planblick.yaml --kubeconfig="./kube_config.yaml"
                rm -f ./kube_config.yaml
            """
        }
    }
}
