#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT


curl --location --request POST 'http://kong.planblick.svc:8001/services/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "planblick-website",
  "url": "http://planblick-website.planblick.svc:80"
}'


curl --location --request POST 'http://kong.planblick.svc:8001/services/planblick-website/routes/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "preserve_host": true,
  "protocols": ["https"],
  "https_redirect_status_code": 301,
  "hosts": ["planblick.com", "www.planblick.com", "impressum.planblick.com", "datenschutz.planblick.com", "stagingwww.planblick.com"]
}'

nginx