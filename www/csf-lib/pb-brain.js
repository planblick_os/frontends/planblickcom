import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {pbInit} from "./pb-init.js?v=[cs_version]";
import {fireEvent} from "./pb-functions.js?v=[cs_version]";
import {PbEventActions} from "../pb-event-actions.js?v=[cs_version]";
import {PbAccount} from "./pb-account.js?v=[cs_version]";
import "./pb-eventlistener.js?v=[cs_version]";

export class pbBrain {
    constructor() {
        this.circuitBaseUrl = CONFIG.CIRCUIT_BASE_URL
        this.circuitJson = undefined
        if (!pbBrain.instance) {
            pbBrain.instance = this;
        }

        return pbBrain.instance;
    }

    boot(circuit = "main-circuit", needsToBeLoggedIn = true) {
        console.log("booting PbBrain")

        new pbInit(needsToBeLoggedIn).now()
        document.addEventListener("system-initialized", function circuitLoadListener(event) {
            document.removeEventListener("system-initialized", circuitLoadListener)

            pbBrain.instance.loadCircuit(circuit)
            window.jQuery(".loading").hide()
            window.jQuery("#content").show()
        })

        document.addEventListener("js_sources_loaded", function circuitLoadListener(event) {
            if (needsToBeLoggedIn) {
                new PbAccount().checkLogin()
            }
        })

        document.addEventListener("resetCircuit", function circuitLoadListener(event) {
            PbEventActions.instance.getByName("resetCircuit")()
        })
    }

    loadCircuit(circuitName) {
        var settings = {
            "url": this.circuitBaseUrl + circuitName + ".json?v=[cs_version]",
            "method": "GET",
            "timeout": 0,
            "success": function (response) {
                fireEvent("circuitLoaded", {"name": circuitName, "data": response})
                pbBrain.instance.circuitJson = response
                pbBrain.instance.processCircuit()
            }.bind(this)
        };

        window.jQuery.ajax(settings).done(function (response) {
        });
    }

    processCircuit(circuitJson) {
        let actions = new PbEventActions()
        let actionMapping = {}
        if (!circuitJson)
            circuitJson = pbBrain.instance.circuitJson


        Object.keys(circuitJson["eventmapping"]).forEach(single_event => {
            //console.log("SINGLEEVENT", single_event)
            if (!actions.getByName(single_event)) {
                console.warn("No Action defined for event '" + single_event + "'")
            } else {
                if (!actionMapping[single_event]) {
                    actionMapping[single_event] = actions.getByName(single_event, (myevent) => "")
                } else {
                    console.log("Removing event listener for: ", single_event)
                    document.removeEventListener(single_event, actionMapping[single_event])
                    actionMapping[single_event] = actions.getByName(single_event, (myevent) =>  "")
                }
                console.log("Adding event listener for: ", single_event)
                document.addEventListener(single_event, actionMapping[single_event])
            }

            circuitJson["eventmapping"][single_event].forEach(triggeredEvent => {
                //console.log("TRIGGEREDEVENT", triggeredEvent)

                if (!actions.getByName(single_event)) {
                    console.warn("No Action defined for event '" + single_event + "'")
                } else {
                    if (!actionMapping[single_event]) {
                        actionMapping[single_event] = actions.getByName(single_event, (myevent) => fireEvent(triggeredEvent, myevent.data))
                    } else {
                        console.log("Removing event listener for: ", single_event)
                        document.removeEventListener(single_event, actionMapping[single_event])
                        actionMapping[single_event] = actions.getByName(single_event, (myevent) => fireEvent(triggeredEvent, myevent.data))
                    }
                    console.log("Adding event listener for: ", single_event)
                    document.addEventListener(single_event, actionMapping[single_event])
                }


                if (actions.getByName(triggeredEvent) == false) {
                    console.warn("No Action defined for event '" + triggeredEvent + "'")
                } else {
                    if (!actionMapping[triggeredEvent]) {
                        actionMapping[triggeredEvent] = actions.getByName(triggeredEvent)
                    } else {
                        console.log("Removing event listener for: ", triggeredEvent)
                        document.removeEventListener(triggeredEvent, actions.getByName(triggeredEvent))
                        actionMapping[triggeredEvent] = actions.getByName(triggeredEvent)
                    }
                    console.log("Adding event listener for: ", triggeredEvent)
                    document.addEventListener(triggeredEvent, actionMapping[triggeredEvent])
                }
            })
        })
        fireEvent("circuitProcessed", {"name": circuitJson.circuit_name, "data": circuitJson})
        if (circuitJson.eventmapping["initialCircuitAction"]) {
            circuitJson.eventmapping["initialCircuitAction"].forEach(action => {
                fireEvent(action)
            })
        }
    }
}