import {CONFIG} from '../pb-config.js?v=[cs_version]';
import {getCookie, fireEvent} from './pb-functions.js?v=[cs_version]';
import {PbAccount} from "./pb-account.js?v=[cs_version]";

export class socketio {
    constructor() {
        if (socketio.instance) {
            return socketio.instance;
        }
        socketio.instance = this;
        socketio.instance.namespace = "/"

        socketio.instance.commandhandlers = {
            fireEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            fireBackendEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            showAlert: function (args) {
                alert(args.text)
            },
            forcereload: function (args) {
                console.log(args)
                let reason = args.reason
                alert("Die Seite muss wegen " + reason + " aktualisiert werden.\nNach bestätigen dieser Meldung erledigen wir das automatisch für Sie.")
                location.reload();
            },
            reloadPage: function (args) {
                location.reload();
            },
            refreshAccessRights: function (args) {
                new PbAccount().refreshPermissions()
            }
        }

        return socketio.instance
    }

    init() {
        //
        console.log("Listening for messages on:" + CONFIG.SOCKET_SERVER + socketio.instance.namespace)
        socketio.instance.socket = io(CONFIG.SOCKET_SERVER + socketio.instance.namespace, {
            path: CONFIG.SOCKETIO_SUBPATH + '/socket.io',
            query: {apikey: getCookie("apikey")}
        });
        var onevent = socketio.instance.socket.onevent;
        socketio.instance.socket.onevent = function (packet) {
            var args = packet.data || [];
            onevent.call(this, packet);    // original call
            packet.data = ["*"].concat(args);
            onevent.call(this, packet);      // additional call to catch-all
        };

        socketio.instance.socket.on('connect_error', function () {
            fireEvent("socketio_cannotconnect")
        })

        socketio.instance.socket.on('connect', function () {
            console.log("Connected to socketio server")
            fireEvent("socketio_connected")
            socketio.instance.socket.emit('join', {room: "tmp_global"});
            if (getCookie("consumer_name"))
                socketio.instance.socket.emit('join', {room: getCookie("consumer_name")});
            if (getCookie("consumer_id"))
                socketio.instance.socket.emit('join', {room: getCookie("consumer_id")});
            if (getCookie("username"))
                socketio.instance.socket.emit('join', {room: decodeURIComponent(getCookie("username"))});
        });
        socketio.instance.socket.on("playresponse", (input) => {
            socketio.instance.playByteArray(input)
        });

        socketio.instance.socket.on("*", function (event, data) {
            console.log("Received message from socketio:", event, data)
            fireEvent(event, data)
            if (typeof (data) == "string") {
                data = JSON.parse(data)
            }

            var payload = null;
            if (typeof (data.data) == "undefined" && data["data"]) {
                payload = JSON.parse(data["data"])
            } else {
                payload = data.data;
            }
            if (!payload) {
                payload = data
            }

            if (event == "command" && socketio.instance.commandhandlers[payload.command]) {
                socketio.instance.commandhandlers[payload.command](payload.args)
                //socketio.instance.commandhandlers[data.command](data.args)
            }
        });

        return self
    }

    joinRoom(room) {
        socketio.instance.socket.emit('join', {room: decodeURIComponent(room)});
    }

    playByteArray(arrayBuffer) {
        if (!socketio.instance.context) {
            socketio.instance.context = new AudioContext();
        }
        socketio.instance.context.decodeAudioData(arrayBuffer, function (buffer) {
            socketio.instance.play(buffer);
        });
    }

    play(buffer) {
        // Create a source node from the buffer
        var source = socketio.instance.context.createBufferSource();
        source.buffer = buffer;
        // Connect to the final output node (the speakers)
        source.connect(socketio.instance.context.destination);
        // Play immediately
        source.start(0);
        source.onended = function () {

        }
    }

}
