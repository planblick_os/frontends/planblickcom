import {socketio} from "./pb-socketio.js?v=[cs_version]";
import {getCookie} from "./pb-functions.js?v=[cs_version]";

export class pbImageRecognition {
    constructor() {
        if (!pbImageRecognition.instance) {
            document.addEventListener("command", function (event) {
                console.log("recognition.numberplate", event.data)
                document.getElementById("snapshotsFromServer").src = event.data.args.src
                console.log("Recognized number:", event.data.args.number)
                document.getElementById("numberplate").value = event.data.args.number
            })

            pbImageRecognition.instance = this;
        }


        return pbImageRecognition.instance;
    }

    recognizeNumberplate(image) {
        new socketio().socket.emit("command", {
            command: "recognition.numberplate",
            args: {"src": image.src, "reply_to_room": getCookie("username")},
            room: "tmp_worker"
        })
    }
}