import {fireEvent} from "./pb-functions.js?v=[cs_version]";
import {PbEventActions} from "../pb-event-actions.js?v=[cs_version]";
import {pbBatch} from "./pb-batch.js?v=[cs_version]";
import {pbBrain} from "./pb-brain.js?v=[cs_version]";

export class pbDecisionmaker {
    constructor() {
        if (!pbDecisionmaker.instance) {
            pbDecisionmaker.instance = this;
            pbDecisionmaker.instance.intentions = new pbBrain().circuitJson["intentions"]
        }

        return pbDecisionmaker.instance;

    }

    getIntentionForEventType(eventType, transcribedText, event) {
        transcribedText = transcribedText.toLowerCase()
        let handled = false
        if(pbDecisionmaker.instance.intentions[eventType]) {
            let intentions = pbDecisionmaker.instance.intentions[eventType]
            console.log(intentions.invocations)
            intentions.invocations.some(function (element, index, array) {
                let invocationWords = element.split(" ")
                if (invocationWords.every(v => transcribedText.includes(v.toLowerCase()))) {
                    console.log("Fire Event:", intentions.triggeredEvents[index])
                    handled = true
                    fireEvent(intentions.triggeredEvents[index], event.data)
                }
            })
        }
        if(!handled) {
            console.warn("No intentions found for: ", event.data.type)
            fireEvent("noIntentionFound", event.data.source_event)
        }
    }

    decideForSpeechRecognitionResult(task_id) {
        let handler_function = function (event) {
            let matched_at_least_once = false
            document.removeEventListener("speechRecognitionResult", handler_function)
            let transcribedText = event.data["result"]
            if (transcribedText) {

                console.log("TRANSCRIBED", transcribedText)

                if (["abbrechen"].some(v => transcribedText.includes(v))) {
                    fireEvent("visitorAborted")
                    return
                }
                if (task_id === event.data["task_id"]) {
                    if (event.data["task_id"].startsWith("syncInputValueGathering_")) {
                        matched_at_least_once = true
                        let mycallback = function () {
                            new pbBatch().resolve("fill_input_with_id_" + event.data["source_event"]["data"]["element_id"])
                        }
                        PbEventActions.instance.getByName("syncInputValueGathering", mycallback)({
                            "id": event.data["source_event"]["data"]["element_id"],
                            "value": transcribedText
                        })

                    } else {
                        matched_at_least_once = true
                        pbDecisionmaker.instance.getIntentionForEventType(event.data.source_event.type, transcribedText, event)
                    }
                }
                if (!matched_at_least_once) {
                    fireEvent("noAudioInWhenExpected", event.data.source_event)
                }
            } else {
                fireEvent("audioInputNotUnderstood", event.data.source_event)
            }

        }
        document.addEventListener("speechRecognitionResult", handler_function)
    }
}