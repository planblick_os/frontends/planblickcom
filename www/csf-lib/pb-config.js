export var BASECONFIG = {}
BASECONFIG.DEFAULT_LOGIN_REDIRECT = "/"
BASECONFIG.JS_CACHE_ACTIVE = true
BASECONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 5000
BASECONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
BASECONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
BASECONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
BASECONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "./js/pb/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "./js/pb/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
]
BASECONFIG.SOCKETIO_SUBPATH = "/socket"
BASECONFIG.NEWS = []

if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    BASECONFIG.CIRCUIT_BASE_URL = "http://localhost:8080/circuits/"
    BASECONFIG.API_BASE_URL = "http://localhost:8000"
    BASECONFIG.APP_BASE_URL = "http://localhost:7000"
    BASECONFIG.SOCKET_SERVER = "http://localhost:8000"
    BASECONFIG.SOCKETIO_SUBPATH = "/socket"
    BASECONFIG.COOKIE_DOMAIN = "localhost"
    BASECONFIG.LOGIN_URL = "http://localhost:7000/login/"
    BASECONFIG.REGISTER_URL = "http://localhost:8080/register.html"
} else {
    console.log("Using LIVE-Config")
    BASECONFIG.CIRCUIT_BASE_URL = "./circuits/"
    BASECONFIG.API_BASE_URL = "https://thor.planblick.com"
    BASECONFIG.APP_BASE_URL = "https://www.serviceblick.com"
    BASECONFIG.APP_STARTPAGE = "/"
    BASECONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    BASECONFIG.COOKIE_DOMAIN = "www.serviceblick.com"
    BASECONFIG.LOGIN_URL = "https://www.serviceblick.com/login/"
    BASECONFIG.REGISTER_URL = "https://www.serviceblick.com/registration/"
}
