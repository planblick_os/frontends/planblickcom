import {PbAccount} from "./csf-lib/pb-account.js?v=[cs_version]";
import {getCookie, fireEvent} from "./csf-lib/pb-functions.js?v=[cs_version]";

export class LoginModule {
    actions = {}
    constructor() {
        if (!LoginModule.instance) {
            this.initListeners()
            this.initActions()
            LoginModule.instance = this;
        }

        return LoginModule.instance;
    }

    initActions() {
        this.actions["loginButtonClicked"] = function(myevent, callback=() => "") {
            new PbAccount().getapikey(false, function(apikey){alert("Login successfull, got apikey of:" + apikey)})
            callback(myevent)
        }
    }

    initListeners() {
        window.jQuery("#register_button").click(function(event) {
            event.preventDefault()
            fireEvent("loginButtonClicked", {"login": window.jQuery("#login").val(), "password": window.jQuery("#password").val()})
        })
    }
}