import {CONFIG} from "../pb-config.js?v=[cs_version]"
import {getCookie, isEmail, makeId, showNotification} from "./pb-functions.js?v=[cs_version]"
import {pbPermissions} from "./pb-permission.js?v=[cs_version]"

export class PbAccount {
    permissions = {}

    constructor() {
        if (!PbAccount.instance) {
            PbAccount.instance = this
            new pbPermissions()
                .getAbsoluteRightsOfRole("user." + getCookie("username")).then(
                (response) => {
                    if (response != undefined) {
                        PbAccount.instance.permissions = response.rules
                        PbAccount.instance.handlePermissionAttributes()
                    }
                })
        }

        return PbAccount.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            PbAccount.instance.getLogins((response) => {
                PbAccount.instance.logins = response
                resolve(response)
            })
        })
    }

    refreshPermissions() {
        new pbPermissions()
            .getAbsoluteRightsOfRole("user." + getCookie("username")).then(
            (response) => {
                PbAccount.instance.permissions = response.rules
                PbAccount.instance.handlePermissionAttributes()
            })
    }

    hasPermission(permission) {
        if (PbAccount.instance.permissions[permission] != undefined
            && PbAccount.instance.permissions[permission].action.includes("allow")
        ) {
            return true
        } else {
            return false
        }
    }

    handlePermissionAttributes() {
        for (let [permission, value] of Object.entries(PbAccount.instance.permissions)) {
            if (PbAccount.instance.hasPermission(permission)) {
                console.log(permission)
                $("[data-permission='" + permission + "']").removeClass('hidden')
                $("[data-permission='!" + permission + "']").addClass('hidden')
            } else {
                $("[data-permission='" + permission + "']").addClass('hidden')
                $("[data-permission='!" + permission + "']").removeClass('hidden')
            }
        }
    }

    sendInvitationLink(email, success = () => {
        showNotification("Invitation sent")
    }, error = (message) => {
        showNotification("Invitation sending failed")
    }) {
        return new Promise(function (resolve, reject) {
            if (!isEmail(email)) {
                error("not an email address")
                return false
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/login_invite",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"username": email}),
                "success": function (response) {
                    success(response);
                    resolve(response)
                },
                "error": function (response) {
                    error(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    startRegistrationProcess(login, password, successCallback, errorCallback) {
        return new Promise(function (resolve, reject) {
            var data = {"username": login, "password": password}

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + "/registration",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                },
                "processData": false,
                "data": JSON.stringify(data),
                "success": function (response) {
                    successCallback(response);
                    resolve(response)
                },
                "error": function (response) {
                    errorCallback(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    startLostPasswordProcess(login, successCallback, errorCallback) {
        return new Promise(function (resolve, reject) {
            var data = {"username": login}

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + "/init_credentials_lost_process",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                },
                "processData": false,
                "data": JSON.stringify(data),
                "success": function (response) {
                    successCallback(response);
                    resolve(response)
                },
                "error": function (response) {
                    errorCallback(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    getLogins(callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_users",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                }
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    deleteAccount(consumer_id, callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteAccount",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"id": consumer_id}),
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    deleteLogin(loginName, callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteLogin",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"username": loginName}),
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    createLogin(username, password, successCallback = () => {
        alert("Login creation successfull")
    }, errorCallback = () => {
        alert("Login creation failed")
    }) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/newlogin",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey"),
                },
                "data": JSON.stringify({"username": username, "password": password}),
                "error": errorCallback
            }

            window.jQuery.ajax(settings).done(function (response) {
                successCallback()
                document.addEventListener("notification", function _(event) {
                    if (event.data.data.args.correlation_id == response.taskid) {
                        if (event.data.data.event == "loginCreationFailed") {
                            reject()
                            errorCallback()
                            console.log("loginCreationFailed", event.data.data.response)
                        }
                        if (event.data.data.event == "loginCreated") {
                            resolve()
                            successCallback()
                        }

                        document.removeEventListener("notification", _)
                    }
                })
            })
        })
    }

    checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/has_valid_login") {
        return new Promise(function (resolve, reject) {
            console.log("Checking login...")
            let url_for_redirect_after_login = CONFIG.LOGIN_URL + '?r=' + window.location.href
            let apikey = window.jQuery.cookie("apikey")
            if (typeof (apikey) == "undefined") {
                let redirect_file = "index.html"
                let current_url = window.location.pathname
                let current_filename = current_url.substring(current_url.lastIndexOf('/') + 1)

                if (redirect_file != current_filename) {
                    window.jQuery.blockUI({
                        blockMsgClass: 'alertBox',
                        message: 'Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...',
                        timeout: 3000,
                        baseZ: 9999999999999999,
                        onUnblock: function () {
                            $(location).attr('href', url_for_redirect_after_login)
                        }
                    })
                }
            } else {
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": CONFIG.API_BASE_URL + health_endpoint,
                    "method": "GET",
                    "headers": {
                        "apikey": window.jQuery.cookie("apikey"),
                        "Cache-Control": "no-cache"
                    },
                    "success": function (response) {
                        setTimeout(PbAccount.instance.checkLogin, checkagainTime, isLoginPage, checkagainTime)
                        if (isLoginPage) {
                            PbAccount.instance.redirectDialog()
                        }
                        resolve(response)
                    },
                    "error": function (response) {
                        if (!isLoginPage) {
                            window.jQuery.blockUI({
                                blockMsgClass: 'alertBox',
                                message: 'Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...',
                                timeout: 3000,
                                baseZ: 9999999999999999,
                                onUnblock: function () {
                                    $(location).attr('href', url_for_redirect_after_login)
                                }
                            })
                        }
                        reject(response)
                    }
                }
                window.jQuery.ajax(settings)
            }
        })
    }

    logout(redirect = null, message = null) {
        return new Promise(function (resolve, reject) {
            let apikey = window.jQuery.cookie("apikey")
            let result = false

            let postUrl = CONFIG.API_BASE_URL + "/logout"
            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                contentType: 'application/json;charset=UTF-8',
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('apikey', apikey)
                },
                success: function (response) {
                    let realRedirect = '/login/'
                    let timeout = 500
                    if (redirect == false) {
                        redirect = "#"
                        timeout = 60000000
                    }
                    if ((redirect != null && redirect != undefined) && redirect.length > 0) {
                        realRedirect = redirect
                    }
                    let realMessage = "Sie wurden abgemeldet."
                    if (message != null) {
                        realMessage = message
                    }

                    if (typeof (response) != "undefined") {
                        window.jQuery.removeCookie("apikey", {path: '/'})

                        window.jQuery.blockUI({
                            blockMsgClass: 'alertBox',
                            message: realMessage,
                            timeout: timeout,
                            onUnblock: function () {
                                $(location).attr('href', realRedirect)
                            }
                        })
                    } else {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        window.jQuery.blockUI({blockMsgClass: 'alertBox', message: realMessage})

                    }
                    resolve()
                },
                error: function () {
                    window.jQuery.removeCookie("apikey", {path: '/'})
                    window.jQuery.blockUI({blockMsgClass: 'alertBox', message: realMessage})
                    reject()
                }
            })
        })
    }

    getapikey(doRedirect = true, successCallback = () => "") {
        return new Promise(function (resolve, reject) {
            let apikey = window.jQuery.cookie("apikey")
            console.debug(apikey)
            let username = document.getElementById("login").value
            let password = document.getElementById("password").value

            window.jQuery.blockUI({blockMsgClass: 'alertBox', message: 'Ihre Zugangsdaten werden überprüft ...'})
            let postUrl = CONFIG.API_BASE_URL + "/login"

            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                contentType: 'application/json;charset=UTF-8',
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password))
                },
                success: function (response) {
                    if (typeof (response) != "undefined") {
                        console.debug("LOGINRESPONSE", response)
                        window.jQuery.blockUI({
                            blockMsgClass: 'alertBox',
                            message: 'Login successfull',
                            timeout: 1000
                        })
                        window.jQuery.cookie("apikey", response.apikey, {path: '/'})
                        window.jQuery.cookie("consumer_name", response.consumer_name, {path: '/'})
                        window.jQuery.cookie("consumer_id", response.consumer.id, {path: '/'})
                        window.jQuery.cookie("username", response.username, {path: '/'})
                        new pbPermissions().getAbsoluteRightsOfRole("user." + username).then(
                            (response) => {
                                PbAccount.instance.permissions = response.rules
                                if (doRedirect) {
                                    PbAccount.instance.redirectDialog(CONFIG.DEFAULT_LOGIN_REDIRECT)
                                } else {
                                    window.jQuery.blockUI({
                                        blockMsgClass: 'alertBox',
                                        message: 'Successfull login',
                                        timeout: 3000
                                    })
                                }
                                successCallback(response.apikey)
                                resolve(response)
                            })
                    } else {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        window.jQuery.unblockUI()
                        window.jQuery.blockUI({
                            blockMsgClass: 'alertBox',
                            message: 'Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.',
                            timeout: 3000
                        })
                        reject(response)
                    }
                },
                error: function (response) {
                    window.jQuery.removeCookie("apikey", {path: '/'})
                    window.jQuery.unblockUI()
                    window.jQuery.blockUI({
                        blockMsgClass: 'alertBox',
                        message: 'Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.',
                        timeout: 3000
                    })
                    reject(response)
                }
            })
        })
    }

    getapikeyfor(username, password, callback) {
        return new Promise(function (resolve, reject) {
            let postUrl = CONFIG.API_BASE_URL + "/login"
            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                contentType: 'application/json;charset=UTF-8',
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password))
                },
                success: function (response) {
                    console.log("RESPONSE", response)
                    if (typeof (response) != "undefined") {
                        callback(response.apikey)
                        resolve(response)
                    } else {
                        callback()
                        resolve()
                    }
                },
                error: function (response) {
                    callback(false)
                    reject(response)
                }
            })
        })
    }

    redirectDialog(url = null) {
        if (url == null) {
            location.href = "/login/"
        } else {
            location.href = url
        }
    }

    changePassword(username, password, onsuccess = null, onfailure = null) {
        return new Promise(function (resolve, reject) {
            let data = {
                "username": username,
                "password": password
            }
            $.blockUI({blockMsgClass: 'alertBox', message: 'Passwort wird aktualisiert.'});
            var settings = {
                "url": CONFIG.API_BASE_URL + "/update_credentials",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": $.cookie("apikey")
                },
                "data": JSON.stringify(data),
                "success": function () {
                    $.blockUI({blockMsgClass: 'alertBox', message: 'Passwort wurde aktualisiert.'});
                    if (onsuccess != null) {
                        onsuccess()
                        resolve()
                    } else {
                        setTimeout(function () {
                            $.unblockUI();
                        }, 2000)
                    }
                },
                "error": function () {
                    $.blockUI({
                        blockMsgClass: 'alertBox',
                        message: 'Passwort konnte nicht aktualisiert werden, bitte versuchen sie es später noch einmal.'
                    });
                    if (onsuccess != null) {
                        onfailure()
                        reject()
                    } else {
                        setTimeout(function () {
                            $.unblockUI();
                        }, 2000)
                    }
                },
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        })
    }
}