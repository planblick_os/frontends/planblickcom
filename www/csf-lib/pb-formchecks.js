export function validateForm(params, funcs = {}, showErrors = true) {

    //alert("I am in validate form now")
    let result = {}
    let return_value = true

    for (let [elem_id, type] of Object.entries(params)) {
        if (type == "textarea") {
            result[elem_id] = validateField(elem_id)
        } else if (type == "input") {
            result[elem_id] = validateField(elem_id)
        }

        if (funcs[type]) {
            result[elem_id] = funcs[type](elem_id)
        }

        for (let [elem_id, check_result] of Object.entries(result)) {
            if (check_result == false) {
                return_value = false
            }
        }
    }

    if (showErrors) {
        handleGUIonFormError(result)
    } else {
        return result
    }
    return return_value
}

export function validateField(elemId) {

    let fieldWithId = document.getElementById(elemId)
    let checkResult = fieldWithId.checkValidity()
    return checkResult
}

export function handleGUIonFormError(checkResults) {
    for (let [elem_id, result] of Object.entries(checkResults)) {
        let fieldName = elem_id
        let fieldWithId = document.getElementById(fieldName)
        let errorId = document.getElementById(fieldName + "Error")
        if (result == false) {
            errorId.classList.replace("error", "error-visible")
            fieldWithId.classList.add("invalid")
            errorId.setAttribute("aria-hidden", false)
            errorId.setAttribute("aria-invalid", true)
        } else {
            errorId.classList.replace("error-visible", "error")
            fieldWithId.classList.remove("invalid")
            errorId.setAttribute("aria-hidden", true)
            errorId.setAttribute("aria-invalid", false)
        }
    }
}

export function validateLetNumbUnderField(elemId) {
    let fieldWithId = document.getElementById(elemId)
    if (fieldWithId.value == "") {
        //console.log("Error: Username cannot be blank!")
        return false
    }
    let re = /^\w+$/;
    if (!re.test(fieldWithId.value)) {
        //console.log("Error: Username must contain only letters, numbers and underscores!")
        return false
    }
    return true
}

export function isStrongPwd(password) {

    var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowercase = "abcdefghijklmnopqrstuvwxyz";
    var digits = "0123456789";
    var splChars = "!@#$%&*()";
    var ucaseFlag = contains(password, uppercase);
    var lcaseFlag = contains(password, lowercase);
    var digitsFlag = contains(password, digits);
    //var splCharsFlag = contains(password, splChars);

    if (password.length >= 8 && ucaseFlag && lcaseFlag && digitsFlag /*&& splCharsFlag*/)
        return true;
    else
        return false;

}

export function contains(password, allowedChars) {
    for (let i = 0; i < password.length; i++) {
        let char = password.charAt(i);
        if (allowedChars.indexOf(char) >= 0) {
            return true;
        }
    }
    return false;
}
