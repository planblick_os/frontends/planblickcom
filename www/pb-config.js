import {BASECONFIG} from "./csf-lib/pb-config.js?v=[cs_version]";

export const CONFIG = BASECONFIG

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "/csf-lib/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "/csf-lib/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "/csf-lib/vendor/socket.io.js?v=[cs_version]", loaded: null},
]

if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:8040/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.NEWS = [
        {
            news_id: "2021_10_21",
            content_uri: "../news/index.html",
            content_id: "2021_10_21",
            show_in_apps: ["easy2schedule"],
            force_display: false
        },
        {
            news_id: "2020_02_18",
            content_uri: "../news/index.html",
            content_id: "2020_02_18",
            show_in_apps: ["easy2track"],
            force_display: false
        },
    ]
    CONFIG.FROALA_KEY = ""

} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    console.log("Using DEV-Config")
    CONFIG.CIRCUIT_BASE_URL = "http://fk.planblick.com:7000/circuits/"
    CONFIG.APP_BASE_URL = "http://fk.planblick.com:7000"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.FROALA_KEY = ""

} else if (document.location.href.indexOf("stagingwww.planblick.com") != -1) {
    console.log("Using STAGING-Config")
    CONFIG.CIRCUIT_BASE_URL = "//stagingwww.planblick.com/circuits/"
    CONFIG.APP_BASE_URL = "//stagingwww.planblick.com"
    CONFIG.API_BASE_URL = "//odin.planblick.com"
    CONFIG.LOGIN_URL = "//testblick.de/login/"
    CONFIG.SOCKET_SERVER = "//odin.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.NEWS = [
        {
            news_id: "2021_10_21",
            content_uri: "../news/index.html",
            content_id: "2021_10_21",
            show_in_apps: ["easy2schedule"],
            force_display: false
        },
        {
            news_id: "current_value_link",
            content_uri: "../easy2track/client.html",
            content_id: "current_value_link",
            show_in_apps: ["easy2track"],
            force_display: false
        },
    ]
    CONFIG.FROALA_KEY = "wFE7nE5D4D4I4B11A8B5fLUQZf1ASFb1EFRNh1Hb1BCCQDUHnA8B6E5A4B1D3F3A1C8B5=="

} else {
    console.log("Using LIVE-Config")
    CONFIG.CIRCUIT_BASE_URL = "https://www.planblick.com/circuits/"
    CONFIG.APP_BASE_URL = "https://www.planblick.com"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.LOGIN_URL = "https://www.planblick.com/login/"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.FROALA_KEY = "wFE7nE5D4D4I4B11A8B5fLUQZf1ASFb1EFRNh1Hb1BCCQDUHnA8B6E5A4B1D3F3A1C8B5=="
}


