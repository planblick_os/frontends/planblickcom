import {PbBaseEventActions} from "./csf-lib/pb-base-event-actions.js?v=[cs_version]"
import {PbAccount} from "./csf-lib/pb-account.js?v=[cs_version]";
import {Easy2GetherModule} from "./js/csf-load-module.js?v=[cs_version]"


export class PbEventActions extends PbBaseEventActions {
    constructor() {
        super();
        this.initActions()
        if (!PbEventActions.instance) {
            PbEventActions.instance = this;
        }

        return PbEventActions.instance;
    }

    initActions() {
        this.actions["initModuleEasy2Gether"] = function (myevent, callback = () => "") {
            let actions = new Easy2GetherModule().actions
            PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
            callback(myevent)
        }
    }
}


